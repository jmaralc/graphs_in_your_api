# About me...
Javier Martinez Alcantara, Javi.

[LinkedIn](https://www.linkedin.com/in/javier-martinez-alcantara/)

[Some info](https://jmaralc.github.io/)

# Graphs in your api: a simple service with python, based on GraphQL and Neo4j

The idea of this project is to develop a simple use case that provides some insight on how two technologies focused on "graphs" (in the wide sense) are integrated with Python.

⚠️ **IMPORTANT**⚠️
Review all the branches and try to follow the slides to understand the flow of the session.

# Motivation

In the tech. ecosystem out there, there are two technologies that seems to fit well together: graphQL and Neo4j. This is pretty obvious in other communities like the javascript community that has develop neat solutions like [GRANDstack](https://grandstack.io/). Usage of both within pyhon community is growing and the motivation of this session is to contribute with a humble bit to that growth.

The plan is to have a simple use case. While developing together we will check the features and capabilities of these two tecnologies in python.

The use case will be a network of communication towers🗼 that needs to be watched or evaluated to consider which points could be more critical for he communication company.

[Some related slides](https://docs.google.com/presentation/d/1sc4jFZWccGIh74pO0Tqg5giYRgiY0YREgFvuft00zCE/edit?usp=sharing)

# Setup

For this talk you will need python 3.8 and pip installed in your computer. We will use pipenv for installing the packages and keeping the virtual environment. So I recomend you to install it with pip:

```shell
pip install --user --upgrade pipenv
```
Further information about installation [here](https://pipenv-fork.readthedocs.io/en/latest/install.html#installing-pipenv)

# Global Goal

- Spread the knowledge of python
- Promote the use of FastAPI
- Promote the adoption of technologies

# Structure of the repository/project

The repository presents different branches, divided in steps. The higher the step the more complex will be the code and more functionality will provide. We can summarize the branches as:
- Master →Mainly readme
- Step 1 →Initial approach to graphQL in FastAPI
- Step 2 →Complex queries
- Step 3 →Mutations….that is how to modify our data (update,delete)
- Step 4 →Neo4j initial setup
- Step 5 →Performing “complex” operations with neo4j


# How to run it
First set in place pipenv with:

```shell
pipenv install
```

To start the server of any of the branches (except master) just run the next:
```shell
pipenv run uvicorn main:app --app-dir ./CTAS/src --reload
```

# Special thanks to..
For gather energies towards a good idea:
https://landscape.graphql.org/

For creating such nice products:
https://neo4j.com/

For the opportunity to share it with you:
https://codurance.com/

For the nice framework:
https://fastapi.tiangolo.com/